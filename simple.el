(setq gc-cons-threshold 100000000)
(setq read-process-output-max (* 1024 1024))
;;; load theme
(load (concat user-emacs-directory "themes/pm-monokai.el"))
;;; enable loaded theme
(enable-theme 'pmonokai)
;;; change default font
(setq default-frame-alist '((font . "fira mono 16")))
;; Start in fullscreen mode
(add-to-list 'default-frame-alist '(fullscreen . maximized))
(set-frame-parameter nil 'undecorated t)
;; Disable bars and scrollbars
(tool-bar-mode -1)
(menu-bar-mode -1)
(toggle-scroll-bar -1)
(setq inhibit-splash-screen t)
;;; save custom variables elsewhere
(setq custom-file (concat user-emacs-directory "custom.el"))
(load-file custom-file)
;;; initial packge load
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
	;(unless package-archive-contents
	;(package-refresh-contents))
;;; check for use-package, which we will use afterwards
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
;;; change default dashboard
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))
;;; dired icons
(use-package all-the-icons-dired
  :ensure t
  :hook (dired-mode))
;;; use dired as a sidebar
(use-package dired-sidebar
  :bind (("C-x C-n" . dired-sidebar-toggle-sidebar))
  :ensure t
  :commands (dired-sidebar-toggle-sidebar))
;;; make ido vertical
(use-package ido-vertical-mode
  :ensure t
  :hook (ido-mode))
(ido-mode 1)
(ido-vertical-mode 1)
(setq ido-vertical-define-keys 'C-n-and-C-p-only)
(ido-everywhere 1)
;;; add ido completions everywhere
(use-package ido-completing-read+
  :ensure t
  :init)
(icomplete-vertical-mode 1)
(use-package smex
  :ensure t
  :init)
(icomplete-mode 1)

(use-package org-bullets
  :ensure t
  :hook (org-mode))
(setq org-src-fontify-natively t)
;;; add consult
(use-package consult
  :ensure t
  :hook (completion-list-mode . consult-preview-at-point-mode))
(setq completion-styles '(substring))
;;; flycheck config
(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))
;;; company config
(use-package company
  :ensure t
  :init (global-company-mode))
(setq company-idle-delay 0)
(add-to-list 'company-backends 'company-elisp)
;;; lsp server
(use-package eglot
  :ensure t)
(add-to-list 'eglot-server-programs '(c-mode . ("clangd12")))
(use-package expand-region
  :bind ("C-;" . er/expand-region)
  :ensure t)


;;; custom key bindings
(global-set-key "\C-ce" (lambda () (interactive)
     (erc-tls :server "irc.steew.cyou" :port "6697"
	      :nick "steew")))
;; text size management
(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
;; I-search with consult-line
(global-set-key (kbd "C-s") 'consult-line)
(global-set-key (kbd "<escape>") #'god-local-mode)
;; Replace M-x with smart mx (smex)
(global-set-key (kbd "M-x") 'smex)
